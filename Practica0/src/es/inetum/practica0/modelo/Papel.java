package es.inetum.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		this("Papel", PiedraPapelTijeraFactory.PAPEL);
	}

	public Papel(String nombre, int numero) {
		super(nombre, numero);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isMe(int pPiePapelTijera) {
		// TODO Auto-generated method stub
		return pPiePapelTijera == PiedraPapelTijeraFactory.PAPEL;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiePapelTijeraFac) {
		int resul = 0;
		switch (pPiePapelTijeraFac.getNumero()) {
		// le gana
		case PIEDRA:
		case SPOCK:
			resul = 1;
			this.descripcionResultado = "Papel le gano a " + pPiePapelTijeraFac.getNombre();
			break;
		// pierde con
		case TIJERA:
		case LAGARTO:
			resul = -1;
			this.descripcionResultado = "Papel perdi� con " + pPiePapelTijeraFac.getNombre();
			break;
		default:
			resul = 0;
			this.descripcionResultado = "Papel empata con " + pPiePapelTijeraFac.getNombre();
			break;
		}
		return resul;
	}
}
