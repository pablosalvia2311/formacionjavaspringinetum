package es.inetum.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory {

	public Piedra() {
		this("Piedra", PiedraPapelTijeraFactory.PIEDRA);
	}

	public Piedra(String nombre, int numero) {
		super(nombre, numero);
	}

	@Override
	public boolean isMe(int pPiePapelTijera) {
		// TODO Auto-generated method stub
		return pPiePapelTijera == PiedraPapelTijeraFactory.PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiePapelTijeraFac) {
		int resul = 0;
		switch (pPiePapelTijeraFac.getNumero()) {
		case TIJERA:
		case LAGARTO:
			resul = 1;
			this.descripcionResultado = "piedra le gana a " + pPiePapelTijeraFac.getNombre();
			break;

		case PAPEL:
		case SPOCK:
			resul = -1;
			this.descripcionResultado = "piedra pierdi� con " + pPiePapelTijeraFac.getNombre();
			break;

		default:
			resul = 0;
			this.descripcionResultado = "piedra empata con " + pPiePapelTijeraFac.getNombre();
			break;
		}
		return resul;
	}

}
