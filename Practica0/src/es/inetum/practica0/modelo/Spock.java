package es.inetum.practica0.modelo;

public class Spock extends PiedraPapelTijeraFactory {
	public Spock() {
		this("spock", PiedraPapelTijeraFactory.SPOCK);
	}

	public Spock(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == SPOCK;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO completar
		int resul = 0;
		switch (pPiedPapelTijera.getNumero()) {

		case TIJERA:
		case PIEDRA:
			resul = 1;
			this.descripcionResultado = "spock le gana a " + pPiedPapelTijera.getNombre();
			break;

		case PAPEL:
		case LAGARTO:
			resul = -1;
			this.descripcionResultado = "spock perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul = 0;
			this.descripcionResultado = "spock empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
