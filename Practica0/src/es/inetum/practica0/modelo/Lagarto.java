package es.inetum.practica0.modelo;

public class Lagarto extends PiedraPapelTijeraFactory {
	public Lagarto() {
		this("lagarto", PiedraPapelTijeraFactory.LAGARTO);
	}

	public Lagarto(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == LAGARTO;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		// TODO Completar
		int resul = 0;
		switch (pPiedPapelTijera.getNumero()) {
		case PAPEL:
		case SPOCK:
			resul = 1;
			this.descripcionResultado = "lagarto le gana a " + pPiedPapelTijera.getNombre();
			break;

		case PIEDRA:
		case TIJERA:
			resul = -1;
			this.descripcionResultado = "lagarto perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul = 0;
			this.descripcionResultado = "lagarto empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
