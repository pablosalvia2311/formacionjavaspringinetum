package es.inetum.practica0.modelo;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		this("Tijera", PiedraPapelTijeraFactory.TIJERA);
	}

	public Tijera(String nombre, int numero) {
		super(nombre, numero);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isMe(int pPiePapelTijera) {
		return pPiePapelTijera == PiedraPapelTijeraFactory.TIJERA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiePapelTijeraFac) {
		int resul = 0;
		switch (pPiePapelTijeraFac.getNumero()) {
		case PAPEL:
		case LAGARTO:
			resul = 1;
			this.descripcionResultado = "tijera le gana a " + pPiePapelTijeraFac.getNombre();
			break;

		case PIEDRA:
		case SPOCK:
			resul = -1;
			this.descripcionResultado = "tijera perdi� con " + pPiePapelTijeraFac.getNombre();
			break;

		default:
			resul = 0;
			this.descripcionResultado = "tijera empata con " + pPiePapelTijeraFac.getNombre();
			break;
		}
		return resul;
	}

}
