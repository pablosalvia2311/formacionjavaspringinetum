package es.inetum.practica0.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	public final static int PIEDRA = 1;
	public final static int PAPEL = 2;
	public final static int TIJERA = 3;
	public final static int LAGARTO = 4;
	public final static int SPOCK = 5;

	protected String descripcionResultado;

	private static List<PiedraPapelTijeraFactory> elementos;

	protected String nombre;
	protected int numero;

	public PiedraPapelTijeraFactory(String nombre, int numero) {
		super();
		this.nombre = nombre;
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public abstract boolean isMe(int pPiePapelTijera);

	public abstract int comparar(PiedraPapelTijeraFactory pPiePapelTijeraFac);

	public static PiedraPapelTijeraFactory getInstance(int pPiePapelTijera) {
		// el corazon del factory
		// 1ro el padre reconoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		// tengo que agregar los nuevos
		elementos.add(new Lagarto());
		elementos.add(new Spock());

		// es todo el codigo va a ser siemrpe el misom
		for (PiedraPapelTijeraFactory piedraPapelTieraFactory : elementos) {
			if (piedraPapelTieraFactory.isMe(pPiePapelTijera))
				return piedraPapelTieraFactory;

		}
		return null;
	}

}
