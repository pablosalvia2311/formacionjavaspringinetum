<%@page import="java.util.List"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${titulo}</title>
<h1>PROFESOR ${profesor}</h1>
</br>
</head>
<body>
	<ul>
		<c:forEach items="${alumnos}" var="alumnos">
			<li><c:out value="${alumnos}" /></li>
		</c:forEach>
	</ul>

</body>
</html>