package com.domain.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.inetum.practica0.modelo.PiedraPapelTijeraFactory;

@Controller
public class IndexController {

	@RequestMapping("/home")
	public String goIndex() {
		return "Index";
	}

	@RequestMapping("/")
	public String getPresentacion() {
		return "Presentacion";
	}

	@RequestMapping("/listado")
	public String goListado(Model model) {
		List<String> alumnos = new ArrayList<String>();
		alumnos.add("Pablo");
		alumnos.add("Pepa");
		alumnos.add("Juan");

		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel casas");
		model.addAttribute("alumnos", alumnos);

		return "Listado";
	}

	@RequestMapping("/juego/{nombre}")
	public String goPiedraPapelTijera(@PathVariable("nombre") String nombre, Model model) {
		List<PiedraPapelTijeraFactory> opciones = new ArrayList<PiedraPapelTijeraFactory>();

		for (int i = 1; i < 6; i++)
			opciones.add(PiedraPapelTijeraFactory.getInstance(i));

		model.addAttribute("nombre", nombre);
		model.addAttribute("opciones", opciones);

		return "PiedraPapelTijera";
	}

	@RequestMapping("/resolverJuego")
	public String goResolverJuego(@RequestParam(required = false) Integer selOpcion, Model model) {

		PiedraPapelTijeraFactory computadora = PiedraPapelTijeraFactory
				.getInstance((int) (Math.random() * 100 % 5 + 1));

		PiedraPapelTijeraFactory jugador = PiedraPapelTijeraFactory.getInstance(selOpcion.intValue());

		jugador.comparar(computadora);

		model.addAttribute("jugador", jugador);
		model.addAttribute("computadora", computadora);
		model.addAttribute("resultado", jugador.getDescripcionResultado());

		return "MostrarResultado";
	}
}
