package com.domain.util.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.domain.util.ConnectionManager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConnectionManagerTest {

	Connection conexion;

	@BeforeEach
	void setUp() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/inetum", "root", "Pablo1234");
	}

	@AfterEach
	void tearDown() throws SQLException {
		conexion.close();
	}

	@Test
	void testConnect() throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		assertTrue(true);
	}

	@Test
	void testDesconectar() throws SQLException, ClassNotFoundException {
		ConnectionManager.conectar(); // apunta a null si no pongo conectar primero
		ConnectionManager.desconectar();
		assertTrue(true);
	}

	@Test
	void testGetConnection() {
		assertNotNull(ConnectionManager.getConnection());

	}
}
